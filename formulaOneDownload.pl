#!/usr/bin/env perl

#    formulaOneDownload.pl: downloads formula one information
#    Copyright (C) 2014 Andrew Schaumberg
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use diagnostics;

for(my $year = 2014; $year >= 1950; $year--) {
  my $url_base = "http://www.formula1.com/results/season";
  open(my $fh, "wget -O- $url_base/$year/ |") or die $!;
  while (<$fh>) {
    if ($_ =~ /<td><a href="\/results\/season\/$year\/(\d+)\/">[^<]+<\/a><\/td>/) {
      my $race = int($1);
      #print("$1\n");
      sleep(1);
      open(my $country_fh, "wget -O- $url_base/$year/$race/ |") or die $!;
      #Pos No      Driver                Team           Laps Time/Retired Grid Pts
      #1   3  Daniel Ricciardo  Red Bull Racing-Renault 70   1:53:05.058  4    25
      #2   14 Fernando Alonso   Ferrari                 70   +5.2 secs    5    18
      #3   44 Lewis Hamilton    Mercedes                70   +5.8 secs    22   15
#raw html
#        <div class="contentContainer">^M
#            <table class="raceResults" cellpadding="0" cellspacing="0" summary="">^M
#                <tr>^M
#                    <th>Pos</th>^M
#                    <th>No</th>^M
#                    <th>Driver</th>^M
#                    <th>Team</th>                    ^M
#^M
#                    ^M
#                    <th>Laps</th>^M
#                    ^M
#                    ^M
#                    <th>Time/Retired</th>^M
#                    ^M
#                    ^M
#                    <th>Grid</th>^M
#                    <th title="Points">Pts</th>^M
#                    ^M
#                    ^M
#                    ^M
#                </tr>^M
#    ^M
#        <tr>^M
#            <td>1</td>^M
#            <td>3</td>^M
#            <td nowrap="nowrap"><a href="/results/driver/2014/857.html">Daniel Ricciardo</a></td>^M
#            <td nowrap="nowrap"><a href="/results/team/2014/2995.html">Red Bull Racing-Renault</a></td>            ^M
#^M
#            ^M
#            <td>70</td>^M
#            ^M
#^M
#            <td>1:53:05.058</td>^M
#         ^M
#            ^M
#            ^M
#            <td>4</td>^M
#            <td>25</td>^M
#            ^M
#            ^M
#            ^M
#        </tr>^M
      my $mode = 0;
      my ($pos, $no, $driver, $team, $laps);
      while (<$country_fh>) {
        if ($mode == 0) {
          if ($_ =~ /<th>Pos<\/th>/) {
            #print("POS<1>\n");
            $mode = 1;
          }
        } elsif ($mode >= 1 and $_ =~ /<tr>/) {
          $pos = $no = $driver = $team = $laps = undef;
          #print("POS<2>\n");
          $mode = 2;
        } elsif ($mode == 2) {
          if ($_ =~ /<td>(\d+)<\/td>/) {
            #print("POS<3>\n");
            $pos = int($1);
            $mode = 3;
          }
        } elsif ($mode == 3) {
          if ($_ =~ /<td>(\d+)<\/td>/) {
            #print("POS<4>\n");
            $no = int($1);
            $mode = 4;
          }
        } elsif ($mode == 4) {
          if ($_ =~ /<td nowrap="nowrap"><a href="[^"]+">([-_ a-zA-Z]+)<\/a><\/td>/) {
            #print("POS<5>\n");
            $driver = $1;
            $mode = 5;
          }
        } elsif ($mode == 5) {
          if ($_ =~ /<td nowrap="nowrap"><a href="[^"]+">([-_ a-zA-Z]+)<\/a><\/td>/) {
            #print("POS<6>\n");
            $team = $1;
            if (defined($pos) and
                defined($no) and
                defined($driver) and
                defined($team)) {
              print("$year\t$race\t$pos\t$no\t$driver\t$team\n");
            }
            $mode = 1;
          }
        }
      }
      #exit;
    }
  }
  #exit;
  sleep(1);
}


